<?php
/**
 * @file
 * Template for the Content Score Report block.
 */
?>
<aside class="content-score-report__block">
  <h3 class="content_score_report__headline">
    <?php print t('Content Score Report'); ?>
    <a href="#" class="content-score-report__loading"><i class="fa fa-refresh"></i></a>
  </h3>
  <div class="content-score-report__scores">
    <div class="content-score-report__msg"></div>
    <h3 class="content-score-report__headline"><a href="#" class="content-score-report__edit"><i class="fa fa-edit"></i></a> <span></span></h3>
    <div class="content-score-report__table">
      <div class="content-score-report__row content-score-report__score-avg_time">
        <div class="content-score-report__cell content-score-report__cell-header">
          <?php print t('Average Time'); ?>:
          <span class="content-score-report__info"></span>
        </div>
        <div class="content-score-report__cell">
          <span class="content-score-report__score"></span>
        </div>
      </div>
      <div class="content-score-report__row content-score-report__score-events">
        <div class="content-score-report__cell content-score-report__cell-header">
          <?php print t('Events'); ?>:
          <span class="content-score-report__info"></span>
        </div>
        <div class="content-score-report__cell">
          <span class="content-score-report__score"></span>
        </div>
      </div>
      <div class="content-score-report__row content-score-report__score-open_rate">
        <div class="content-score-report__cell content-score-report__cell-header">
          <?php print t('Open Rate'); ?>:
          <span class="content-score-report__info"></span>
        </div>
        <div class="content-score-report__cell">
          <span class="content-score-report__score"></span>
        </div>
      </div>
      <div class="content-score-report__row content-score-report__score-pageviews">
        <div class="content-score-report__cell content-score-report__cell-header">
          <?php print t('Pageviews'); ?>:
          <span class="content-score-report__info"></span>
        </div>
        <div class="content-score-report__cell">
          <span class="content-score-report__score"></span>
        </div>
      </div>
      <div class="content-score-report__row content-score-report__score-total">
        <div class="content-score-report__cell content-score-report__cell-header">
          <?php print t('Score'); ?>:
          <span class="content-score-report__info"><?php print t('Based on a weighted total.'); ?></span>
        </div>
        <div class="content-score-report__cell">
          <span class="content-score-report__score"></span>
        </div>
      </div>
    </div>
    <div class="content-score-report__form">
      <label for="content-score-report__type" class="content-score-report__label"><?php print t('Type'); ?></label>
      <div class="content-score-report__clear">
        <label class="content-score-report__radio"><input type="radio" id="content-score-report__typeAcquisition" name="content-score-report__type"><?php print t('Acquisition'); ?></label>
        <label class="content-score-report__radio"><input type="radio" id="content-score-report__typeRetention" name="content-score-report__type"><?php print t('Retention'); ?></label>
      </div>
      <label for="content-score-report__startDate" class="content-score-report__label"><?php print t('Start Date'); ?></label>
      <input type="date" id="content-score-report__startDate" placeholder="<?php t('Start date'); ?>" class="content-score-report__input">
      <label for="content-score-report__endDate" class="content-score-report__label"><?php print t('End Date'); ?></label>
      <input type="date" id="content-score-report__endDate" placeholder="<?php t('End date'); ?>" class="content-score-report__input">
      <input type="button" id="content-score-report__submit" value="Re-calculate Scores" class="content-score-report__button">
    </div>
    <div class="content-score-report__details">
      <?php print t('Scores calculated based on data from <span class="content-score-report__date content-score-report__startDateTxt content-score-report__edit"></span> through <span class="content-score-report__date content-score-report__endDateTxt content-score-report__edit"></span>.'); ?>
    </div>
  </div>
</aside>
