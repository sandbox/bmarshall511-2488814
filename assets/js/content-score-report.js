/**
 * @file
 * Content Score Report block AJAX functionality.
 */

(function($) {
  "use strict";

  // Create the Content Score Report container.
  var contentScoreReport = {
    // Content Score Report settings.
    settings: Drupal.settings.contentScoreReport,

    // Resets the Content Score Report to the loading position.
    resetBlock: function(callback) {
      this.startDate = this.block.attr("data-startdate");
      this.endDate   = this.block.attr("data-enddate"),
      this.type      = this.block.attr("data-type");

      // Hide any messages.
      $(".content-score-report__msg", this.block).html("").hide();

      // Hide the form.
      $(".content-score-report__form", this.block).hide();

      // Hide the score content.
      $(".content-score-report__scores", this.block).slideUp();

      // Hide the loading icon.
      $(".content-score-report__loading", this.block).addClass("fa-spin").hide();

      // Show open rate score row.
      $(".content-score-report__score-open_rate", this.block).show();

      // Show events score row.
      $(".content-score-report__score-events", this.block).show();

      // Hide the score info text.
      $(".content-score-report__tip-info", this.block).hide();

      // Set the start & end dates.
      $("#content-score-report__startDate", this.block).val(this.startDate);
      $("#content-score-report__endDate", this.block).val(this.endDate);

      $(".content-score-report__startDateTxt", this.block).html(this.startDate);
      $(".content-score-report__endDateTxt", this.block).html(this.endDate);

      // Set type.
      if ("acquisition" === this.type) {
        $("#content-score-report__typeAcquisition", this.block).prop("checked", true);
        $(".content-score-report__headline span", this.block).html("Acquisition Scores");
      }
      else if ("retention" === this.type) {
        $("#content-score-report__typeRetention", this.block).prop("checked", true);
        $(".content-score-report__headline span", this.block).html("Retention Scores");
      }

      // Call the callback.
      callback();
    },

    // Retrieve the Content Score Report data.
    retrieveData: function(callback) {

      // Set the AJAX request URL.
      var requestURL = "/node/content-score-report/" + this.block.data("nid");

      if (typeof this.block.data("type") !== "undefined") {
        requestURL += "/" + this.type;

        if (typeof this.startDate !== "undefined") {
          requestURL += "/" + this.startDate;

          if (typeof this.endDate !== "undefined") {
            requestURL += "/" + this.endDate;
          }
        }
      }

      // Show the loading icon.
      $(".content-score-report__loading", this.block).fadeIn();

      // Send the AJAX request.
      $.getJSON(requestURL).done(function(data) {
        // Stop the loading icon.
        $(".content-score-report__loading", this.block).removeClass("fa-spin");

        // Call the callback.
        callback(data);
      });
    },

    // Render the Content Score Report data in the block.
    renderScores: function(data) {
      console.log(data);

      // Check for any errors.
      if (data.errors.length === 0 && typeof data.score !== "undefined") {

        // Define the scores.
        var scores = {
          'avg_time' : { 'data': data.gaData.pageviews.avg_time_formatted },
          'pageviews': { 'data': data.gaData.pageviews.unique_pageviews }
        };

        // Determine Content Score Report type scores & weights.
        if ("acquisition" == data.type) {
          $(".content-score-report__score-open_rate", this.block).hide();
          scores.avg_time.weight  = this.settings.acquisition.scoreWeights.avg_time;
          scores.pageviews.weight = this.settings.acquisition.scoreWeights.pageviews;
          scores.events           = {
             'weight': this.settings.acquisition.scoreWeights.events,
             'data': data.gaData.events.unique_events
          };
        }
        else if ("retention" == data.type) {
          $(".content-score-report__score-events", this.block).hide();
          scores.avg_time.weight  = this.settings.retention.scoreWeights.avg_time;
          scores.pageviews.weight = this.settings.retention.scoreWeights.pageviews;
          scores.open_rate        = {
             'weight': this.settings.retention.scoreWeights.open_rate,
             'data'  : data.mcData.open_rate_formatted
          };
        }

        // Loop through & render each score.
        var totalScore = 0;
        $.each(scores, function(key, obj) {
          if (typeof data.score[key] !== "undefined") {
            var row = $(".content-score-report__score-" + key, this.block),
                value;

            switch (key) {
              case 'open_rate':
                value = scores[key].data + "%";
                break;

              case 'pageviews':
                value = scores[key].data.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,") + " (unique pageviews)";
                break;

              case 'events':
                value = scores[key].data.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,") + " (unique events)";
                break;

              default:
                value = scores[key].data;
            }

            $(".content-score-report__score", row).html(data.score[key]);
            $(".content-score-report__info", row).html(value);

            totalScore += (data.score[key] * scores[key].weight);
          }
        });

        // Render the total score.
        $(".content-score-report__score", $(".content-score-report__score-total", this.block)).html(totalScore);

      }
      else {

        // Add each error message.
        $.each(data.errors, function(key, value) {
          $(".content-score-report__msg", this.block).append(value + " ");
        });

        // Hide the Content Score Report table & show the errors.
        $(".content-score-report__table", this.block).hide();
        $(".content-score-report__details", this.block).hide();
        $(".content-score-report__msg", this.block).show();
      }

      // Show the Content Score Report.
      $(".content-score-report__scores", this.block).slideDown();
    }
  };

  // On page ready.
  $(function() {
    var app = contentScoreReport;

    // Initialize each Content Score Report block.
    $(".content-score-report__block").each(function() {
      app.block = $(this);

      // Set the defaults.
      contentScoreReport.settings.startDate = isNaN(contentScoreReport.settings.startDate) ? contentScoreReport.settings.startDate : contentScoreReport.settings.startDate * 1000;
      contentScoreReport.settings.endDate   = isNaN(contentScoreReport.settings.endDate) ? contentScoreReport.settings.endDate : contentScoreReport.settings.endDate * 1000;

      var startDate  = new Date(contentScoreReport.settings.startDate),
          endDate    = new Date(contentScoreReport.settings.endDate),
          startDay   = ("0" + startDate.getDate()).slice(-2),
          endDay     = ("0" + endDate.getDate()).slice(-2),
          startMonth = ("0" + (startDate.getMonth() + 1)).slice(-2),
          endMonth   = ("0" + (endDate.getMonth() + 1)).slice(-2);

      startDate = startDate.getFullYear() + "-" + startMonth + "-" + startDay;
      endDate   = endDate.getFullYear() + "-" + endMonth + "-" + endDay;

      app.block.attr("data-nid", contentScoreReport.settings.nid);
      app.block.attr("data-type", contentScoreReport.settings.type);
      app.block.attr("data-startdate", startDate);
      app.block.attr("data-enddate", endDate);

      $("#content-score-report__startDate", app.block).attr("min", startDate);
      $("#content-score-report__startDate", app.block).attr("max", endDate);

      $("#content-score-report__endDate", app.block).attr("min", startDate);
      $("#content-score-report__endDate", app.block).attr("max", endDate);

      // Render the Content Score Report block.
      app.resetBlock(function() { app.retrieveData(function(data) {
        app.renderScores(data);
      }); });

      // Event handler for date form.
      $(".content-score-report__edit", app.block).click(function(e) {
        e.preventDefault();
        $(".content-score-report__form", app.block).slideToggle();
      });

      // Event handler for loading icon.
      $(".content-score-report__loading").click(function() {
        // Render the Content Score Report block.
        app.resetBlock(function() { app.retrieveData(function(data) {
          app.renderScores(data);
        }); });
      });

      // Event handler for form.
      $("#content-score-report__submit", app.block).click(function() {

        // Update the settings.
        if ($("#content-score-report__typeAcquisition").is(":checked")) {
          app.block.attr("data-type", 'acquisition');
        }
        else if ($("#content-score-report__typeRetention").is(":checked")) {
          app.block.attr("data-type", 'retention');
        }

        app.block.attr("data-startdate", $("#content-score-report__startDate", app.block).val());
        app.block.attr("data-enddate", $("#content-score-report__endDate", app.block).val());

        // Render the Content Score Report block.
        app.resetBlock(function() {
          app.retrieveData(function(data) {
            app.renderScores(data);
          });
        });
      });
    });
  });
})(jQuery);
