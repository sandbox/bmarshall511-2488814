<?php
/**
 * @file
 * Administrative page callbacks for the Content Score Report module.
 */

/**
 * General configuration form for the Content Score Report module.
 */
function content_score_report_admin_settings() {
  // Google Analytics configuration.
  $form['ga_configuration'] = array(
    '#type'  => 'fieldset',
    '#title' => t('GA Configuration'),
  );

  // Google Analytics project ID.
  $form['ga_configuration']['content_score_report_ga_project_id'] = array(
    '#type'          => 'textfield',
    '#title'         => t('GA Project ID'),
    '#description'   => t("Enter the project's Project ID from the Google Developers Console."),
    '#required'      => TRUE,
    '#default_value' => variable_get('content_score_report_ga_project_id'),
  );

  // Google Analytics email address.
  $form['ga_configuration']['content_score_report_ga_email_address'] = array(
    '#type'          => 'textfield',
    '#title'         => t('GA Email Address'),
    '#description'   => t("Enter the APIs Email Address from the Google Developers Console."),
    '#required'      => TRUE,
    '#default_value' => variable_get('content_score_report_ga_email_address'),
  );

  // Google Analytics private key.
  $form['ga_configuration']['content_score_report_ga_private_key_file'] = array(
    '#type'          => 'file',
    '#title'         => t('GA Private Key File'),
    '#description'   => t("Copy and paste the text from the GA private key JSON file downloaded from the Google Developers Console."),
  );

  // Google Analytics view ID.
  $form['ga_configuration']['content_score_report_ga_view_id'] = array(
    '#type'          => 'textfield',
    '#title'         => t('GA View ID'),
    '#description'   => t("Enter the default GA account view ID to be used when gathering data."),
    '#required'      => TRUE,
    '#default_value' => variable_get('content_score_report_ga_view_id'),
  );

  // Google Analytics event category.
  $form['ga_configuration']['content_score_report_ga_event_category'] = array(
    '#type'          => 'textfield',
    '#title'         => t('GA Event Category'),
    '#description'   => t("Enter the default GA event category to be used for events data."),
    '#required'      => TRUE,
    '#default_value' => variable_get('content_score_report_ga_event_category'),
  );

  // Google Analytics event action.
  $form['ga_configuration']['content_score_report_ga_event_action'] = array(
    '#type'          => 'textfield',
    '#title'         => t('GA Event Action'),
    '#description'   => t("Enter the default GA event action to be used for events data."),
    '#required'      => TRUE,
    '#default_value' => variable_get('content_score_report_ga_event_action'),
  );

  // Google Analytics event label.
  $form['ga_configuration']['content_score_report_ga_event_label'] = array(
    '#type'          => 'textfield',
    '#title'         => t('GA Event Label'),
    '#description'   => t("Enter the default GA event label to be used for events data. Use [node_path] for the current node path."),
    '#required'      => TRUE,
    '#default_value' => variable_get('content_score_report_ga_event_label'),
  );

  // Scores configuration.
  $form['scores_configuration'] = array(
    '#type'  => 'fieldset',
    '#title' => t('Scores Configuration'),
  );

  // Retention Scores configuration.
  $form['scores_configuration']['retention'] = array(
    '#type'  => 'fieldset',
    '#title' => t('Retention Scores'),
  );

  // Retention scores pageviews weight.
  $form['scores_configuration']['retention']['content_score_report_scores_weight_retention_pageviews'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Pageviews Weight'),
    '#description'   => t("Enter a number to define the weight given to this score."),
    '#required'      => TRUE,
    '#default_value' => variable_get('content_score_report_scores_weight_retention_pageviews', 0.2),
  );

  // Retention scores average time weight.
  $form['scores_configuration']['retention']['content_score_report_scores_weight_retention_avg_time'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Average Time Weight'),
    '#description'   => t("Enter a number to define the weight given to this score."),
    '#required'      => TRUE,
    '#default_value' => variable_get('content_score_report_scores_weight_retention_avg_time', 0.45),
  );

  // Retention scores open rate weight.
  $form['scores_configuration']['retention']['content_score_report_scores_weight_retention_open_rate'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Open Rate Weight'),
    '#description'   => t("Enter a number to define the weight given to this score."),
    '#required'      => TRUE,
    '#default_value' => variable_get('content_score_report_scores_weight_retention_open_rate', 0.35),
  );

  // Acquisition scores configuration.
  $form['scores_configuration']['acquisition'] = array(
    '#type'  => 'fieldset',
    '#title' => t('Acquisition Scores'),
  );

  // Acquisition scores pageviews weight.
  $form['scores_configuration']['acquisition']['content_score_report_scores_weight_acquisition_pageviews'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Pageviews Weight'),
    '#description'   => t("Enter a number to define the weight given to this score."),
    '#required'      => TRUE,
    '#default_value' => variable_get('content_score_report_scores_weight_acquisition_pageviews', 0.2),
  );

  // Acquisition scores average time weight.
  $form['scores_configuration']['acquisition']['content_score_report_scores_weight_acquisition_avg_time'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Average Time Weight'),
    '#description'   => t("Enter a number to define the weight given to this score."),
    '#required'      => TRUE,
    '#default_value' => variable_get('content_score_report_scores_weight_acquisition_avg_time', 0.2),
  );

  // Acquisition scores events weight.
  $form['scores_configuration']['acquisition']['content_score_report_scores_weight_acquisition_events'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Events Weight'),
    '#description'   => t("Enter a number to define the weight given to this score."),
    '#required'      => TRUE,
    '#default_value' => variable_get('content_score_report_scores_weight_acquisition_events', 0.6),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Submit',
  );

  return $form;
}

/**
 * Validate the Content Score Report admin settings form.
 */
function content_score_report_admin_settings_validate($form, &$form_state) {
  $file = file_save_upload('content_score_report_ga_private_key_file', array(
    // Validate extensions.
    'file_validate_extensions' => array('p12'),
  ));
  // If the file passed validation:
  if ($file) {
    // Move the file into the Drupal file system.
    if ($file = file_move($file, 'private://')) {
      // Save the file for use in the submit handler.
      $form_state['storage']['content_score_report_ga_private_key_file'] = $file;
    }
    else {
      form_set_error('content_score_report_ga_private_key_file', t("Failed to write the uploaded file to the site's file folder."));
    }
  }
  else {
    form_set_error('content_score_report_ga_private_key_file', t('No file was uploaded.'));
  }
}

/**
 * Submit handler for the Content Score Report admin settings form.
 */
function content_score_report_admin_settings_submit($form, &$form_state) {
  $file = $form_state['storage']['content_score_report_ga_private_key_file'];
  // We are done with the file, remove it from storage.
  unset($form_state['storage']['content_score_report_ga_private_key_file']);
  // Make the storage of the file permanent.
  $file->status = FILE_STATUS_PERMANENT;
  // Save file status.
  file_save($file);

  // Update the variables.
  variable_set('content_score_report_ga_project_id', $form_state['values']['content_score_report_ga_project_id']);
  variable_set('content_score_report_ga_client_id', $form_state['values']['content_score_report_ga_client_id']);
  variable_set('content_score_report_ga_email_address', $form_state['values']['content_score_report_ga_email_address']);
  variable_set('content_score_report_ga_private_key_file', $file->uri);
  variable_set('content_score_report_ga_view_id', $form_state['values']['content_score_report_ga_view_id']);
  variable_set('content_score_report_ga_event_category', $form_state['values']['content_score_report_ga_event_category']);
  variable_set('content_score_report_ga_event_action', $form_state['values']['content_score_report_ga_event_action']);
  variable_set('content_score_report_ga_event_label', $form_state['values']['content_score_report_ga_event_label']);
  variable_set('content_score_report_scores_weight_retention_pageviews', $form_state['values']['content_score_report_scores_weight_retention_pageviews']);
  variable_set('content_score_report_scores_weight_retention_avg_time', $form_state['values']['content_score_report_scores_weight_retention_avg_time']);
  variable_set('content_score_report_scores_weight_retention_open_rate', $form_state['values']['content_score_report_scores_weight_retention_open_rate']);
  variable_set('content_score_report_scores_weight_acquisition_events', $form_state['values']['content_score_report_scores_weight_acquisition_events']);
  variable_set('content_score_report_scores_weight_acquisition_avg_time', $form_state['values']['content_score_report_scores_weight_acquisition_avg_time']);
  variable_set('content_score_report_scores_weight_acquisition_pageviews', $form_state['values']['content_score_report_scores_weight_acquisition_pageviews']);

  // Set a response to the user.
  drupal_set_message(t('The form has been submitted and the GA Private Key File has been saved: @filename', array('@filename' => $file->filename)));
}
