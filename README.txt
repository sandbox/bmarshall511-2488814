INTRODUCTION
------------
The Content Score Report module provides a block to displays a node's
acquisition and retention scores.

For more information on the Content Score Report library, please check out the
repo: https://github.com/bmarshall511/content-score-report

REQUIREMENTS
------------

This module requires the following modules:

* MailChimp Campaign (https://www.drupal.org/project/mailchimp_campaign)

This module requires the following libraries:

* Content Score Report PHP Library (https://github.com/bmarshall511/content-score-report-php-library)
* Google API PHP Client (https://github.com/google/google-api-php-client)
  The only files you need are `autoload.php` and `src/`. All other files can be
  deleted.


INSTALLATION
------------
* Install as you would normally install a contributed Drupal module.
  See: https://drupal.org/documentation/install/modules-themes/modules-7 for
  further information.
* Download then upload the required libraries to you `sites/all/libraries`
  folder.
* The `content-score-report` library should only contain the
  `content-score-report.class.php`, all other files can be removed (i.e.
  `sites/all/libraries/content-score-report/content-score-report.class.php`)

CONFIGURATION
-------------
* Add the Content Score Report block in Administration &raquo; Structure &raquo;
  Blocks.
* Set the module's configuration in Administration &raquo; Configuration &raquo;
  Content Score Report. You'll need to have setup and configured a Google
  Project through the developer's console
  (https://console.developers.google.com) and created a client ID using a
  service account.

MAINTAINERS
-----------
Current maintainers:
* Ben Marshall (bmarshall) - https://www.drupal.org/u/bmarshall
* Chhab Pachabhaiya (pachabhaiya) - https://www.drupal.org/u/pachabhaiya

## Planned Enhancements

* Setup Grunt to compile CSS & JS.
* Allow the ability to change the GA view ID in the block.
* Allow the ability to change the GA event definitions in the block.
* Allow score weights to be configurable.
* Add string translations for JS.
* Convert to using the officially supported Mailchimp API.
* Add validation to admin settings form.
